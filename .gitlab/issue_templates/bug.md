# Summary
(Summarize the bug encountered concisely)

### Expected behavior 
(What you should see instead)

### Current behavior
(What actually happens)


# Steps to reproduce
(How one can reproduce the issue - this is very important)

# Input Files
(Paste all relevant input files, settings.xml, dplist.dat, gruppen.dat the original .var file and the resulting .var file)

# Logs
(Paste any relevant logs - mainly the output of the console after issuing the conversion or any excpetion messages - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)


# Possible fixes
(If you can, link to the line of code that might be responsible for the problem)

/label ~bug
