# Introduction
the burklimat_converter is used to generate ST variable files which can be utilized by the new burklimat version.

# Issue tracker
Please check the [contributing](CONTRIBUTING.md) guide for details.

# Build
Simply download the Visual Studio Solution and build it on your PC.
