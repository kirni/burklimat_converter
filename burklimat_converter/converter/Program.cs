﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace burklimat
{
    class Program
    {
        /*only one argument aaccepted; argument is path to settings file; if no valid arguments search vor "settings.xml" in exe path, if none found promt user to type path*/
        static void Main(string[] args)
        {
            //generate sample files for user as reference
            writeSamples();

            //settings file is always in same dir as exe unless defined different with args
            FileInfo settingsFile = new FileInfo("settings.xml");

            if (args.Length == 1)
            {
                settingsFile = new FileInfo(args[0]);
            }
    
            //ask user to define existing file for settings is no valid settings found so far
            while (settingsFile.Exists == false)
            {
                Console.WriteLine("settings file <<" + settingsFile.FullName + ">> does not exist, enter new path");
                settingsFile = new FileInfo(Console.ReadLine());
            }

            //start conversion
            new Converter(settingsFile).convert();
        }

        /// <summary>
        /// generates a sample file
        /// </summary>
        static void writeSamples()
        {
            /*generate samples*/
            using (TextWriter writer = new StreamWriter("settings_sample.xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Converter.Settings));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                serializer.Serialize(writer, Converter.Settings.generateSample());
            }
        }
    }

    /// <summary>
    /// handles the conversion from legacy burklimat files to a new var file
    /// </summary>
    public class Converter
    {
        /// <summary>
        /// settings for the conversion
        /// </summary>
        public Settings settings { get; private set; }

        /// <summary>
        /// create converter instance with settings stored in settingsFile
        /// </summary>
        /// <param name="settingsFile"></param>
        public Converter(FileInfo settingsFile)
        {
            Console.WriteLine(string.Format("read settings from {0}",settingsFile.FullName));
            loadSettings(settingsFile);
        }

        /// <summary>
        /// create converter instance with sample settings
        /// </summary>
        public Converter()
        {
            Console.WriteLine(string.Format("no settings specified, use defaults"));
            settings = Settings.generateSample();
        }

        /// <summary>
        /// create converter instance with settings
        /// </summary>
        /// <param name="settings"></param>
        public Converter(Settings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// loads settings into converter instance
        /// </summary>
        /// <param name="settingsFile"></param>
        public void loadSettings(FileInfo settingsFile)
        {
            Console.WriteLine(string.Format("read settings from {0}", settingsFile.FullName));
            settings = Settings.load(settingsFile);
        }

        /// <summary>
        /// save settings to a file
        /// </summary>
        /// <param name="settingsFile"></param>
        public void saveSettings(FileInfo settingsFile)
        {
            Console.WriteLine(string.Format("save settings to {0}", settingsFile.FullName));
            settings.save(settingsFile);
        }

        /// <summary>
        /// convert files according to internal settings
        /// </summary>
        public void convert()
        {
            Console.WriteLine("start converting");

            //read the all variables from old var file
            Console.WriteLine(string.Format("deserialize variables from {0}", settings.srcVarPath));
            VarFile varFile = new VarFile();
            varFile.deserialize(settings.srcVarPath);

            //read objects from dplist object
            Console.WriteLine(string.Format("deserialize dplist from {0}", settings.dplistPath));
            Dplist dplist = new Dplist();
            dplist.deserialize(settings.dplistPath, settings);

            //read objects from gruppen object
            Console.WriteLine(string.Format("deserialize gruppen from {0}", settings.gruppenPath));
            Gruppen gruppen = new Gruppen();
            gruppen.deserialize(settings.gruppenPath, settings);

            //merge identified values from dplist into variables
            Console.WriteLine(string.Format("merge dplist into variables"));
            varFile.merge(dplist);

            //merge identified values into variables
            Console.WriteLine(string.Format("merge gruppen into variables"));
            varFile.merge(gruppen);

            //save variables to file
            varFile.save(settings.destVarPath);

            //create alarms
            Console.WriteLine(string.Format("create alarms"));
            AlarmXConfig alarmXConfig = new AlarmXConfig();
            alarmXConfig.generate(settings.alarmxcorePath, varFile, settings.alarmxconfig);

            Console.WriteLine("conversion done");         
        }

        /*key is the name of the group, value is the actual group, containing of variable names*/
        /// <summary>
        /// dictionary containing gruppen; name of gruppe is the key, the gruppe instance is the value
        /// </summary>
        public class Gruppen : Dictionary<string, Gruppen.Gruppe>
        {
            /// <summary>
            /// a gruppe is just a set of unique strings that are merged into variable values
            /// </summary>
            public class Gruppe : HashSet<string> { }            

            /// <summary>
            /// creates gruppen from the file src based on the settings
            /// </summary>
            /// <param name="src"></param>
            /// <param name="settings"></param>
            public void deserialize(string src, Settings settings)
            {
                Console.WriteLine("attempt to deserialize gruppen from {0}", src);

                string data = File.ReadAllText(src);

                Console.WriteLine("remove comments");
                data = Regex.Replace(data, @";.+", "");

                Console.WriteLine("remove whitespace");
                data = Regex.Replace(data, @"\n+", "");

                Console.WriteLine("match groupes");
                //match string from $ to end of line
                MatchCollection groupMatches = Regex.Matches(data, "([^$]+)");

                foreach (Match groupMatch in groupMatches)
                {
                    //match until ",
                    Match match = Regex.Match(groupMatch.Value, ".+?(?=\",)");

                    /*match is a valid name*/
                    if (match.Value != string.Empty)
                    {
                        string groupname = string.Format("'{0}'", match.Value);
                        Console.WriteLine("group {0} found", groupname);

                        Gruppe group = new Gruppe();
                        //matc hbetween "@ and ",
                        MatchCollection varMatches = Regex.Matches(groupMatch.Value, "(?<=\"@).+?(?=\",)");

                        foreach (Match var in varMatches)
                        {
                            Console.WriteLine("add variable {0} to group {1}", var.Value, groupname);
                            group.Add(string.Format("{0}", var.Value));
                        }

                        this.Add(groupname, group);
                    }
                }
                Console.WriteLine("deserialized gruppen from {0}", src);
            }
        }

        /// <summary>
        /// class representing dplist dataobject
        /// </summary>
        public class Dplist
        {
            public Dictionary<string, Element> elements { get; private set; }

            public Dplist()
            {
                elements = new Dictionary<string, Element>();
            }

            public void deserialize(string src, Settings settings)
            {
                elements = Element.getElements(File.ReadAllText(src), settings);
            }

            public class Element
            {
                public string name { get; private set; }
                public string text { get; private set; }
                public Vis vis { get; private set; }
                public Pab pab { get; private set; }

                public static Dictionary<string, Element> getElements(string data, Settings settings)
                {
                    /*get var data*/
                    string elementData = Regex.Replace(data, @";.+", "");
                    elementData = Regex.Replace(elementData, @"\n+", "");
                    elementData = Regex.Replace(elementData, Regex.Match(elementData, "(.*?)(?=\"@)").Value, "");

                    MatchCollection elementMatches = Regex.Matches(elementData, "([^@]+)");

                    Lookup.Table visTable = null;
                    if (settings.visLookup != null)
                    {
                        visTable = new Lookup.Table(settings.visLookup.elements);
                    }

                    Lookup.Table pabTable = null;
                    if (settings.pabLookup != null)
                    {
                        pabTable = new Lookup.Table(settings.pabLookup.elements);
                    }

                    Dictionary<string, Element> elements = new Dictionary<string, Element>();
                    foreach (Match match in elementMatches)
                    {
                        if (match.Value.Length <= 1)
                        {
                            continue;
                        }
                        string srcName = Regex.Match(match.Value, ".+?(?=\")").Value;

                        elements.Add(srcName, new Element()
                        {
                            name = srcName,
                            text = Regex.Match(match.Value, "(?<=~).+?(?=\")").Value,
                            pab = new Pab(Regex.Match(match.Value, "(?<=#PAB).+?(?=\")").Value, settings.pabIgnore, pabTable),
                            vis = new Vis(Regex.Match(match.Value, "(?<=#VIS).+?(?=\")").Value, settings.visIgnore, visTable),
                        });
                    }

                    return elements;
                }

                public class Vis
                {
                    public List<VarFile.Variable.Value> values;
                    public Vis(string src, List<string> ignore = null, Lookup.Table table = null)
                    {
                        //line contains no =; skip it
                        if (src.Contains('=') == false)
                        {
                            return;
                        }

                        MatchCollection variablesMatches = Regex.Matches(src, "([^ ]+)");

                        //no matches; skip this line
                        if (variablesMatches.Count < 1)
                        {
                            return;
                        }
                        values = new List<VarFile.Variable.Value>();

                        foreach (Match match in variablesMatches)
                        {
                            MatchCollection set = Regex.Matches(match.Value, "([^=]+)");

                            //there must to be exactly two matches -> the name and the value; skip this line
                            if (set.Count != 2)
                            {
                                Console.WriteLine("Warning: Name or value of <<"+ src + ">> not found; pleae check syntax; line is skipped");
                                return;
                            }

                            string srcName = set[0].Value;
                            string srcValue = set[1].Value;

                            //skip this element if it is part of the ognore list
                            if (ignore != null)
                            {
                                if (ignore.Contains(srcName) == true)
                                {
                                    continue;
                                }
                            }

                            values.Add(new VarFile.Variable.Value()
                            {
                                Name = table == null ? srcName : table.convert(srcName),
                                Data = table == null ? srcValue : table.convert(srcValue),
                            });
                        }
                    }

                    class VisNameUnknown : Exception
                    {
                    }
                }

                public class Pab : Vis
                {
                    public Pab(string src, List<string> ignore = null, Lookup.Table table = null) : base(src, ignore, table) { }
                }

                public class Lookup
                {
                    protected Table table;
                    public List<Element> elements;

                    public class Element
                    {
                        [XmlAttribute]
                        public string input;
                        [XmlAttribute]
                        public string output;
                    }

                    public static Lookup getSample()
                    {
                        return new Lookup()
                        {
                            elements = new List<Element>()
                            {
                                new Element() { input = "W.Einh", output = "Unit" },
                                new Element() { input = "W.Kpos", output = "KposXW" },
                                new Element() { input = "Y.Kpos", output = "KposY" },
                                new Element() { input = "Einh", output = "Unit" },
                                new Element() { input = "Kpos", output = "Kpos" },
                            }
                        };
                    }

                    public class Table : Dictionary<string, Element>
                    {
                        public Table(List<Element> elements) : base()
                        {
                            foreach (Element element in elements)
                            {
                                this.Add(element.input, element);
                            }
                        }

                        public string convert(string input)
                        {
                            if (this.ContainsKey(input) == true)
                            {
                                return this[input].output;
                            }

                            //return input when no conversion entry found
                            return input;
                        }
                    }
                }
            }
        }

        public class VarFile
        {
            public VariablesDictionary<Variable> variables { get; private set; }
            public VariablesDictionary<Variable.Retain> retainVariables { get; private set; }
            public VariablesDictionary<Variable.Constant> constants { get; private set; }

            public class VariablesDictionary<T> : Dictionary<string, T> where T : Variable, new()
            {
                public void serialize(TextWriter writer)
                {
                    foreach (KeyValuePair<string, T> set in this)
                    {
                        Variable variable = set.Value;

                        string line = "\t" + variable.Name + " : " + variable.Type;// +" := (";

                        /*assign directly when only one variable value*/
                        if (variable.Values.Count == 1)
                        {
                            line += " := ";

                            Variable.Value value = variable.Values.First().Value;

                            /*value is a string when it contains something else than digits, has no bool value and has no (0) assignment*/
                            if (variable.Type != "BOOL" && value.Data != "(0)")
                            {
                                double f;
                                /*make data value a string when it is not a number and when it dous not contain ' already*/
                                if (Double.TryParse(value.Data, out f) == false && value.Data.Contains("'") == false)
                                {
                                    value.Data = ("'" + value.Data + "'");
                                }
                            }

                            line += value.Data;
                        }
                        else if (variable.Values.Count > 0)
                        {
                            line += " := (";

                            foreach (KeyValuePair<string, Variable.Value> pair in variable.Values)
                            {
                                Variable.Value value = pair.Value;

                                /*can not check for BOOL in struct; check for value true or false -> might be an issue if a string eqchals TURE or FALSE*/
                                if (value.Data != "TRUE" && value.Data != "FALSE")
                                {
                                    double f;
                                    /*make data value a string when it is not a number and when it dous not contain ' already*/
                                    if (Double.TryParse(value.Data, out f) == false && value.Data.Contains("'") == false)
                                    {
                                        value.Data = ("'" + value.Data + "'");
                                    }
                                }
                                line += (value.Name + ":=" + value.Data + ",");
                            }

                            //remove last "," 
                            line = line.Remove(line.Length - 1);
                            line += ")";
                        }

                        line += (";(*" + variable.Comment + "*)");
                        writer.WriteLine(line);
                    }
                }
            }

            public void save(string dest)
            {
                Console.WriteLine("attempt to save var file {0}", dest);

                using (TextWriter writer = File.CreateText(dest))
                {
                    /*write variables*/
                    Console.WriteLine("write variables");
                    if (variables != null && variables.Count > 0)
                    {
                        Variable var = new Variable();
                        writer.WriteLine(var.START_TAG);
                        variables.serialize(writer);
                        writer.WriteLine(var.END_TAG);
                    }

                    /*write retain variables*/
                    Console.WriteLine("write retain variables");
                    if (retainVariables != null && retainVariables.Count > 0)
                    {
                        Variable.Retain ret = new Variable.Retain();
                        writer.WriteLine(ret.START_TAG);
                        retainVariables.serialize(writer);
                        writer.WriteLine(ret.END_TAG);
                    }

                    /*write constant variables*/
                    Console.WriteLine("write constant variables");
                    if (constants != null && constants.Count > 0)
                    {
                        Variable.Constant cons = new Variable.Constant();
                        writer.WriteLine(cons.START_TAG);
                        constants.serialize(writer);
                        writer.WriteLine(cons.END_TAG);
                    }

                }

                Console.WriteLine("var file {0} saved", dest);
            }

            public void merge(Dplist src)
            {
                Console.WriteLine("attempt to merge {0} into {1}", src, this);

                foreach (KeyValuePair<string, Dplist.Element> pair in src.elements)
                {
                    Dplist.Element element = pair.Value;
                    Console.WriteLine("attempt to merge {0} into {1}", element, this);

                    if (variables.ContainsKey(element.name) == false)
                    {
                        Console.WriteLine("no variable for {0} found in {1}", element, this);
                        continue;
                    }
                    Variable variable = variables[element.name];

                    /*enter text of the element to Klartext variable*/
                    if (variable.Values.ContainsKey("Klartext") == false)
                    {
                        Console.WriteLine("no Klartext for {0} found", variable);
                        variable.Values.Add("Klartext", new Variable.Value()
                        {
                            Data = element.text,
                            Name = "Klartext"
                        });
                    }
                    else
                    {
                        variable.Values["Klartext"].Data += (" " + element.text);
                    }

                    /*enter PAB variables*/
                    if (element.pab.values != null)
                    {
                        foreach (Variable.Value value in element.pab.values)
                        {
                            if (variable.Values.ContainsKey(value.Name) == true)
                            {
                                variable.Values[value.Name].Data = value.Data;
                            }
                            else
                            {
                                variable.Values.Add(value.Name, value);
                            }
                        }
                    }

                    /*enter VIS variables*/
                    if (element.vis.values != null)
                    {
                        foreach (Variable.Value value in element.vis.values)
                        {
                            if (variable.Values.ContainsKey(value.Name) == true)
                            {
                                variable.Values[value.Name].Data = value.Data;
                            }
                            else
                            {
                                variable.Values.Add(value.Name, value);
                            }
                        }
                    }
                }

                Console.WriteLine("{0} merged into {1}", src, this);
            }
            public void merge(Gruppen src)
            {
                Console.WriteLine("attempt to merge {0} into {1}", src, this);

                foreach (KeyValuePair<string, Gruppen.Gruppe> gruppePair in src)
                {
                    foreach (string variable in gruppePair.Value)
                    {
                        if (variables.ContainsKey(variable))
                        {
                            if (variables[variable].Values.ContainsKey("Gruppe") == false)
                            {
                                variables[variable].Values.Add("Gruppe", new Variable.Value() { Data = gruppePair.Key, Name = "Gruppe" });
                                Console.WriteLine("group {0} added to {1}", gruppePair.Key, variables[variable].Name);
                            }
                            else
                            {
                                Console.WriteLine("{0} contains already a group", variables[variable].Name);
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("variable {0} which is a member of group {1} not found", variable, gruppePair.Key);
                            Console.ResetColor();
                        }
                    }
                }

                Console.WriteLine("{0} merged into {1}", src, this);
            }

            public void deserialize(string src)
            {
                string data = File.ReadAllText(src);

                this.variables = Variable.getElements<Variable>(data);
                this.retainVariables = Variable.getElements<Variable.Retain>(data);
                this.constants = Variable.getElements<Variable.Constant>(data);
            }

            public class Variable
            {
                public string Name { get; set; }
                public string Type { get; set; }
                public string Comment { get; set; }
                public Dictionary<string, Value> Values { get; set; }

                public virtual string START_TAG { get { return "VAR"; } }
                public virtual string END_TAG { get { return "END_VAR"; } }

                public virtual bool containsForbidden(string text)
                {
                    Retain ret = new Retain();
                    Constant cons = new Constant();
                    Variable var = new Variable();

                    if (this.START_TAG != ret.START_TAG && text.Contains(ret.START_TAG))
                    {
                        return true;
                    }

                    if (this.START_TAG != cons.START_TAG && text.Contains(cons.START_TAG))
                    {
                        return true;
                    }

                    return false;
                }

                public virtual void load(string line)
                {
                    Match commentMatch = Regex.Match(line, @"\(\*(.*)\*\)");

                    Match nameMatch = Regex.Match(line, @"(.*?)(?=:)");

                    //try to match assuming there is a value assignment with ":="
                    Match typeMatch = Regex.Match(line, @"(?=:)(.*?)(?=:=)");

                    //if type was not found with value assignment, the type ends with ";"
                    if (typeMatch.Success == false)
                    {
                        typeMatch = Regex.Match(line, @"(?=:)(.*)(?=;)");
                    }

                    Match valuesMatch = Regex.Match(line, @":=(.*?);");

                    /*type is mandatory*/
                    if (typeMatch.Success == false)
                    {
                        throw new VarLineInvalid.NoTypeFound();
                    }
                    /*name is mandatory*/
                    if (nameMatch.Success == false)
                    {
                        throw new VarLineInvalid.NoNameFound();
                    }

                    Name = Regex.Replace(nameMatch.Value, @"\s+", "");
                    Comment = Regex.Replace(commentMatch.Value, @"[\(*\)\s]+", "");
                    Type = Regex.Replace(typeMatch.Value, @"[\s:;]+", "");
                    Type = Regex.Replace(Type, @"OF+", " OF ");

                    /*contains a value*/
                    if (valuesMatch.Value != null && valuesMatch.Value != "")
                    {
                        /*struct value*/
                        if (valuesMatch.Value.Contains("("))
                        {
                            /*whole struct is initialized with 0*/
                            if (valuesMatch.Value.Replace(":=", " ").Replace(" ", "").Replace(";", "") == "(0)")
                            {
                                Values.Add("", new Value()
                                {
                                    Name = "",
                                    Data = "(0)",
                                });
                            }
                            /*struct has a list of assignments*/
                            else
                            {
                                string values = Regex.Replace(valuesMatch.Value, @"[\(\)\s]+", "");

                                foreach (Match match in Regex.Matches(values, "([^,]+)"))
                                {
                                    MatchCollection valueMatches = Regex.Matches(match.Value, "([^:=]+)");
                                    Values.Add(valueMatches[0].Value, new Value()
                                    {
                                        Name = valueMatches[0].Value,
                                        Data = valueMatches[1].Value.Replace("'", "")
                                    });
                                }
                            }
                        }
                        /*basic value*/
                        else
                        {
                            Values.Add("", new Value()
                            {
                                Name = "",
                                Data = valuesMatch.Value.Replace(":=", "").Replace(";", "").Replace(" ", ""),
                            });
                        }
                    }

                }

                public static VariablesDictionary<T> getElements<T>(string data) where T : Variable, new()
                {
                    T element = new T();
                    /*get var data*/
                    Regex r = new Regex("(?=" + element.START_TAG + "\r)(.*?)(?=" + element.END_TAG + "\r)", RegexOptions.Singleline);
                    //Regex r = new Regex(element.START_TAG + "(?!.*RETAIN).*?" + element.END_TAG, RegexOptions.Singleline);

                    string varData = "";

                    foreach (Match match in r.Matches(data))
                    {
                        if (element.containsForbidden(match.Value))
                        {
                            continue;
                        }
                        varData += match.Value;
                    }

                    varData = varData.Replace(element.END_TAG, "");
                    varData = varData.Replace(element.START_TAG, "");

                    using (StringReader reader = new StringReader(varData))
                    {
                        VariablesDictionary<T> variables = new VariablesDictionary<T>();

                        while (true)
                        {
                            string line = reader.ReadLine();
                            if (line == null)
                            {
                                break;
                            }

                            try
                            {
                                element = new T();
                                element.load(line);
                                variables.Add(element.Name, element);
                            }
                            /*go to next line when this one was invalid*/
                            catch (VarLineInvalid)
                            {
                                continue;
                            }
                            catch (Exception e)
                            {
                                throw new Exception(string.Format("error while trying to parse for variable; line <<{0}>>", line), e);
                            }
                        }
                        return variables;
                    }
                }

                public Variable()
                {
                    Values = new Dictionary<string, Value>();
                }

                public class Value
                {
                    public string Name { get; set; }
                    public string Data { get; set; }
                }

                public class VarLineInvalid : Exception
                {
                    public class NoTypeFound : VarLineInvalid
                    {
                    }
                    public class NoNameFound : VarLineInvalid
                    {
                    }
                }

                public class Retain : Variable
                {
                    public override string START_TAG { get { return "VAR RETAIN"; } }
                }
                public class Constant : Variable
                {
                    public override string START_TAG { get { return "VAR CONSTANT"; } }
                }
            }

        }

        public class AlarmXConfig
        {
            public void generate(string dest, VarFile src, AlarmSettings settings)
            {
                //read xml from alarm
                XDocument doc = XDocument.Load(dest);

                List<XElement> alarms = (from el in doc.Descendants()
                                                where (string)el.Attribute("ID") == "mapp.AlarmX.Core.Configuration"
                                                select el).ToList<XElement>();

                XElement alarm;

                //create node for alarms if none is existing
                if (alarms.Count == 0)
                {
                    List<XElement> elemets = (from el in doc.Descendants()
                                             where (string)el.Attribute("Type") == "mpalarmxcore"
                                              select el).ToList<XElement>();

                    alarm = new XElement("Group");
                    alarm.Add(new XAttribute("ID", "mapp.AlarmX.Core.Configuration"));

                    elemets[0].Add(alarm);
                }
                //use first found alarms node
                else
                {
                    alarm = alarms[0];
                }              

                //loop through all variables; skip retain and constants for now!
                foreach (KeyValuePair<string, VarFile.Variable> pair in src.variables)
                {
                    VarFile.Variable var = pair.Value;

                    //loop through all patterns and look for matches
                    foreach (Pattern pattern in settings.patterns)
                    {
                        if (pattern.matches(var))
                        {
                            Console.WriteLine("alarm match {0} found in {1}", pattern, var);
                            alarm.Add(pattern.generate(var));
                        }
                    }               

                }

                int i = 0;
                foreach (XElement element in alarm.Elements())
                {
                    element.Attribute("ID").Value = "[" + i++.ToString() + "]";
                }

                doc.Save(dest);
            }
            public class AlarmSettings
            {
                public List<Pattern> patterns;

                public static AlarmSettings getSample()
                {
                    return new AlarmSettings()
                    {
                        patterns = new List<Pattern>()
                        {
                            Pattern.getSample(),
                            Pattern.getSample(),
                        }
                    };
                }
            }

            public class Pattern
            {
                public AlarmMatch match;
                public string behaviour;
                public string name;
                public string message;

                static int index = 0;

                public bool matches(VarFile.Variable var)
                {
                    return match.matches(var);
                }

                private string applyWildcards(string src, VarFile.Variable var)
                {
                    string result = src;

                    //wildcard #Index
                    result = result.Replace("#Index", index.ToString());

                    //wildcard #PVName
                    result = result.Replace("#PVName", var.Name);

                    //wildcard#PV{XXX}
                    MatchCollection members = Regex.Matches(result, "#PV{.*}");

                    foreach (Match member in members)
                    {
                        string key = member.Value.Replace("#PV{", "").Replace("}", "");
                        if (var.Values.ContainsKey(key))
                        {
                            result = result.Replace(member.Value, var.Values[key].Data);
                            //remove special chars
                            result = result.Replace("\"","");
                            result = result.Replace("\'", "");
                        }
                    }

                    return result;
                }

                public XElement generate(VarFile.Variable var)
                {
                    XElement alarm = new XElement("Group");
                    alarm.Add(new XAttribute("ID", "deadbeef"));

                    XElement name = new XElement("Property");
                    name.Add(new XAttribute("ID", "Name"));
                    name.Add(new XAttribute("Value", applyWildcards(this.name,var)));
                    alarm.Add(name);

                    XElement message = new XElement("Property");
                    message.Add(new XAttribute("ID", "Message"));
                    message.Add(new XAttribute("Value", applyWildcards(this.message, var)));
                    alarm.Add(message);

                    XElement behaviour = new XElement("Property");
                    behaviour.Add(new XAttribute("ID", "Behavior"));
                    behaviour.Add(new XAttribute("Value", applyWildcards(this.behaviour, var)));
                    alarm.Add(behaviour);

                    index++;

                    return alarm;
                }
               
                [XmlInclude(typeof(AlarmMatch.Type))]
                public abstract class AlarmMatch
                {
                    public abstract bool matches(VarFile.Variable var);

                    public string value;
                    public class Type : AlarmMatch
                    {
                        public override bool matches(VarFile.Variable var)
                        {
                            return Regex.IsMatch(var.Type, value);
                        }
                    }
                }

                public static Pattern getSample()
                {                    
                    return new Pattern()
                    {
                        behaviour = "PersistentAlarm",
                        message = "#PV{Klartext} #Index",
                        name = "#PVName",
                        match = new AlarmMatch.Type()
                        {
                            value = "BkAl",
                        },
                    };
                }
            }
        }

        [XmlRootAttribute("Settings")]
        public class Settings
        {
            public string srcVarPath;
            public string destVarPath;
            public string dplistPath;
            public string gruppenPath;
            public string alarmxcorePath;

            public AlarmXConfig.AlarmSettings alarmxconfig;

            public List<String> pabIgnore = new List<string>();
            public List<String> visIgnore = new List<string>();

            public Dplist.Element.Lookup pabLookup;
            public Dplist.Element.Lookup visLookup;

            public override string ToString()
            {
                return string.Format("{0}\n\tSource var path: <{1}>\n\tDestination var path: <{2}>\n\tdplist path: <{3}>\n\tgruppen path: <{4}>\n", this.GetType(), srcVarPath, destVarPath, dplistPath, gruppenPath);
            }

            public static Settings generateSample()
            {
                return new Settings()
                {
                    destVarPath = "destination of the var file which is created",
                    srcVarPath = "full path to the source var file",
                    dplistPath = "full path to the dplist dataobject",
                    gruppenPath = "full path to the gruppen dataobject",
                    pabLookup = Dplist.Element.Lookup.getSample(),
                    visLookup = Dplist.Element.Lookup.getSample(),
                    visIgnore = { "ignre_in_vis_1", "ignre_in_vis_2" },
                    pabIgnore = { "ignore_in_pab_1", "ignore_in_pab_2", },
                    alarmxcorePath = "destination of the generated alarms",
                    alarmxconfig = AlarmXConfig.AlarmSettings.getSample(),
                };
            }

            public static Settings load(FileInfo settingsFile)
            {
                Console.WriteLine("attempt to load settings from " + settingsFile);

                try
                {
                    using (FileStream destFs = new FileStream(settingsFile.FullName, FileMode.Open))
                    {
                        XmlSerializer destSerializer = new XmlSerializer(typeof(Settings));
                        Settings inst = (Settings)destSerializer.Deserialize(destFs);

                        Console.WriteLine(inst + "loaded from " + settingsFile);
                        return inst;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("loading settings from {0} failed", settingsFile);
                    Console.WriteLine(e.Message);

                    for (Exception inner = e.InnerException; inner != null; inner = inner.InnerException)
                    {
                        Console.WriteLine(inner.Message);
                    }

                    Console.WriteLine("use settings sample");

                    Settings inst = Settings.generateSample();

                    Console.WriteLine("attempt to save " + inst + " to " + settingsFile);
                    using (TextWriter writer = new StreamWriter(settingsFile.FullName))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(Converter.Settings));
                        XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                        ns.Add("", "");
                        serializer.Serialize(writer, inst);
                    }
                    Console.WriteLine("saved to {0}", settingsFile);

                    return inst;
                }

            }

            public void save(FileInfo settingsFile)
            {
                Console.WriteLine("attempt to save " + this + " to " + settingsFile);
                using (TextWriter writer = new StreamWriter(settingsFile.FullName))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Converter.Settings));
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    serializer.Serialize(writer, this);
                }
                Console.WriteLine("saved to {0}", settingsFile);
            }
        }
    }
}
