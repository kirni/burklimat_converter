﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

using Microsoft.Win32;

namespace converter_gui
{

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel viewmodel;
        public MainWindow()
        {
            InitializeComponent();

            Console.SetOut(TextWriter.Synchronized(new ControlWriter(consoleOut)));

            viewmodel = new MainWindowViewModel();
            DataContext = viewmodel.settingsViewModel;             

            /*open file browser for click events on the settings browse buttons*/
            srcVarPathBrowsebutton.Click += (object sender, RoutedEventArgs e) =>
            {
                OpenFileDialog fileDialog = new OpenFileDialog();

                if (fileDialog.ShowDialog() == true)
                {
                    viewmodel.settingsViewModel.srcVarPath = fileDialog.FileName;
                }
            };
            destVarPathBrowsebutton.Click += (object sender, RoutedEventArgs e) =>
            {
                OpenFileDialog fileDialog = new OpenFileDialog();

                if (fileDialog.ShowDialog() == true)
                {
                    viewmodel.settingsViewModel.destVarPath = fileDialog.FileName;
                }
            };
            dplistPathBrowsebutton.Click += (object sender, RoutedEventArgs e) =>
            {
                OpenFileDialog fileDialog = new OpenFileDialog();

                if (fileDialog.ShowDialog() == true)
                {
                    viewmodel.settingsViewModel.dplistPath = fileDialog.FileName;
                }
            };
            gruppenPathBrowsebutton.Click += (object sender, RoutedEventArgs e) =>
            {
                OpenFileDialog fileDialog = new OpenFileDialog();

                if (fileDialog.ShowDialog() == true)
                {
                    viewmodel.settingsViewModel.gruppenPath = fileDialog.FileName;
                }
            };
            alarmxcorePathBrowsebutton.Click += (object sender, RoutedEventArgs e) =>
            {
                OpenFileDialog fileDialog = new OpenFileDialog();

                if (fileDialog.ShowDialog() == true)
                {
                    viewmodel.settingsViewModel.alarmxcorePath = fileDialog.FileName;
                }
            };

            /*start conversion*/
            convertButton.Click += async (object sender, RoutedEventArgs e) =>
            {
                //disable button while converting
                ((Button)sender).IsEnabled = false;

                //start conversion acyclically and wait for completion-> GUI remains acessible
                await Task.Run(() => viewmodel.convert(viewmodel.settingsViewModel.settings));

                //enable button again after conversion
                ((Button)sender).IsEnabled = true;

                //scroll output to end after conversion
                consoleOut.ScrollToEnd();
            };

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Laden Sie Daten durch Festlegen der CollectionViewSource.Source-Eigenschaft:
            // settingsViewSource.Source = [generische Datenquelle]
        }      
    }
}
