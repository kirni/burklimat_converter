﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.ComponentModel;

using System.Windows.Threading;


namespace converter_gui
{
    /// <summary>
    /// textwriter which writes the text to a textbox GUI element
    /// </summary>
    public class ControlWriter : TextWriter
    {
        /// <summary>
        /// textbox where the Textwriter is redirected to
        /// </summary>
        private TextBox textbox;

        /// <summary>
        /// textbox is the textbox where the text from the stream is written
        /// </summary>
        /// <param name="textbox"></param>
        public ControlWriter(TextBox textbox)
        {
            this.textbox = textbox;
        }       

        /// <summary>
        /// appends value to the textbox
        /// </summary>
        /// <param name="value"></param>
        public override void Write(char value)
        {
            //Application.Current.Dispatcher.Invoke(() =>
            Application.Current.Dispatcher.Invoke(() =>
            {
                textbox.AppendText(value.ToString());
            });
        }

        /// <summary>
        /// scrolls textbox to end additionally to base.flush()
        /// </summary>
        public override void Flush()
        {
            base.Flush();
            textbox.ScrollToEnd();
        }

        /// <summary>
        /// appends value to the textox
        /// </summary>
        /// <param name="value"></param>
        public override void Write(string value)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                textbox.AppendText(value);
            });
        }

        public override Encoding Encoding
        {
            get { return Encoding.ASCII; }
        }
    }

    /// <summary>
    /// view model for the main window; handles the logic for the GUI
    /// </summary>
    public class MainWindowViewModel
    {
        /// <summary>
        /// settings that are used for the conversion
        /// </summary>
        public SettingsViewModel settingsViewModel { get; set; }

        public MainWindowViewModel()
        {
            Console.WriteLine("load settings");
            FileInfo settingsFile = new FileInfo(SettingsViewModel.settingsFile);

            burklimat.Converter.Settings settings;

            //read settings if the file exists
            if (settingsFile.Exists == true)
            {
                Console.WriteLine("converter settings file found");
                settings = burklimat.Converter.Settings.load(settingsFile);
            }
            //use default settings if no valid settings found
            else
            {
                Console.WriteLine("no converter settings file found; create default file");
                settings = burklimat.Converter.Settings.generateSample();
                settings.save(settingsFile);
            }
            Console.WriteLine("settings loaded");

            //initialize viewmodel with loaded settings
            settingsViewModel = new SettingsViewModel(settings);
        }

        /// <summary>
        /// converts to new burklimat version based on the settings
        /// </summary>
        /// <param name="settings"></param>
        public void convert(burklimat.Converter.Settings settings)
        {
            try
            {
                new burklimat.Converter(settings).convert();
            }
            catch (Exception e)
            {
                Console.WriteLine("conversion failed with excpetion: <<{0}>>",e.Message);

                //write all inner exceptions to the output
                Exception inner = e.InnerException;
                while (inner != null)
                {
                    Console.WriteLine("inner exception excpetion: <<{0}>>", inner.Message);
                    inner = inner.InnerException;
                }
            }
        }

        /// <summary>
        /// containing the settings for the main window view model
        /// </summary>
        public class SettingsViewModel : INotifyPropertyChanged
        {
            /// <summary>
            /// settings of the converter backend
            /// </summary>
            public burklimat.Converter.Settings settings { get; private set; }

            public event PropertyChangedEventHandler PropertyChanged;

            /// <summary>
            /// the settings file does not change
            /// </summary>
            public static string settingsFile
            {
                get
                {
                    return "Settings.xml";
                }
            }

            /// <summary>
            /// path to the source var file
            /// </summary>
            public string srcVarPath
            {
                get
                {
                    return settings.srcVarPath;
                }
                set
                {
                    if (value != srcVarPath)
                    {
                        settings.srcVarPath = value;
                        onPropertyChanged(new PropertyChangedEventArgs("srcVarPath"));
                    }
                }
            }

            /// <summary>
            /// path to the dest par file
            /// </summary>
            public string destVarPath
            {
                get
                {
                    return settings.destVarPath;
                }
                set
                {
                    if (value != destVarPath)
                    {
                        settings.destVarPath = value;
                        onPropertyChanged(new PropertyChangedEventArgs("destVarPath"));
                    }
                }
            }

            /// <summary>
            /// path to the dest par file
            /// </summary>
            public string alarmxcorePath
            {
                get
                {
                    return settings.alarmxcorePath;
                }
                set
                {
                    if (value != alarmxcorePath)
                    {
                        settings.alarmxcorePath = value;
                        onPropertyChanged(new PropertyChangedEventArgs("alarmxcorePath"));
                    }
                }
            }

            /// <summary>
            /// path to the dplist dataobject
            /// </summary>
            public string dplistPath
            {
                get
                {
                    return settings.dplistPath;
                }
                set
                {
                    if (value != dplistPath)
                    {
                        settings.dplistPath = value;
                        onPropertyChanged(new PropertyChangedEventArgs("dplistPath"));
                    }
                }
            }

            /// <summary>
            /// path to the gruppen dataobject
            /// </summary>
            public string gruppenPath
            {
                get
                {
                    return settings.gruppenPath;
                }
                set
                {
                    if (value != gruppenPath)
                    {
                        settings.gruppenPath = value;
                        onPropertyChanged(new PropertyChangedEventArgs("gruppenPath"));
                    }
                }
            }

            /// <summary>
            /// internel settings references to settings
            /// </summary>
            /// <param name="settings"></param>
            public SettingsViewModel(burklimat.Converter.Settings settings)
            {
                this.settings = settings;

                PropertyChanged += (object sender, PropertyChangedEventArgs e) =>
                {
                    Console.WriteLine(string.Format("Setting {0} was changed", e.PropertyName));
                };
                PropertyChanged += (object sender, PropertyChangedEventArgs e) =>
                {
                    settings.save(new FileInfo(settingsFile));
                };
            }

            public void onPropertyChanged(PropertyChangedEventArgs e)
            {
                PropertyChanged?.Invoke(this, e);
            }
        }

    }
}
